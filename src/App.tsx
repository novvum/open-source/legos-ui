import Invoice from './components/Invoice';
import * as React from 'react';
import './App.css';
// import HttpError from './components/HttpError';

class App extends React.Component {
	public render() {
		return (
			<div className='App'>
				{/* <HttpError
					code='404'
					subtitle='Looks like you got lost'
					description={`The page you're looking for doesn't exist or has been moved.`}
					image='https://isomorphic.redq.io/static/media/rob.b34fc52c.png'
					onClick={() => window.open('novvum.io', '_blank')}
					linkText='Back to Novvum'
				/> */}
				<Invoice />
			</div>
		);
	}
}

export default App;
