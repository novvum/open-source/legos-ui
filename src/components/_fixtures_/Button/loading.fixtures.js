import Button from "../../Button";

export default {
  component: Button,
  props: {
    type: "primary",
    loading: true,
    children: "Loading Button"
  }
};
