import Button from "../../Button";
export default {
  component: Button,
  props: {
    type: "primary",
    size: "large",
    children: "Large Button"
  }
};
