import Button from "../../Button";

export default {
  component: Button,
  props: {
    type: "primary",
    icon: "plus",
    shape: "circle"
  }
};
