import Button from "../../Button";

export default {
  component: Button,
  props: {
    type: "primary",
    size: "small",
    children: "Small Button"
  }
};
