import React from "react";
import DemoNotes from "../../Notes";
import messages from "../../IntlMessage/messages";
import { IntlProvider } from "react-intl";

function Notes() {
  return (
    <IntlProvider messages={messages} locale="en">
      <DemoNotes />
    </IntlProvider>
  );
}

export default {
  component: Notes
};
