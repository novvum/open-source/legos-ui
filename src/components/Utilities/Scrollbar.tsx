import React, { ReactChildren } from "react";
import Scrollbar from "react-smooth-scrollbar";

interface ScrollProps {
  id?: any;
  style?: any;
  children?: ReactChildren;
  className?: any;
}

const Scroll: any = ({ id, style, children, className }: ScrollProps) => (
  <Scrollbar
    key={id}
    className={className}
    style={style}
    continuousScrolling={true}
  >
    {children}
  </Scrollbar>
);

export default Scroll;
