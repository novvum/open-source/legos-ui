import { Map } from "immutable";

function clearToken() {
  return localStorage.removeItem("id_token");
}

function getToken() {
  try {
    const idToken = localStorage.getItem("id_token");
    const map = Map({ idToken });
    return map;
  } catch (err) {
    clearToken();
    const map = Map();
    return map;
  }
}

function timeDifference(givenTime: any) {
  givenTime = new Date(givenTime);
  const milliseconds = new Date().getTime() - givenTime.getTime();
  const numberEnding = (num: number) => {
    return num > 1 ? "s" : "";
  };
  const int = (num: number) => (num > 9 ? "" + num : "0" + num);
  const getTime = () => {
    let temp = Math.floor(milliseconds / 1000);
    const years = Math.floor(temp / 31536000);
    if (years) {
      const month = int(givenTime.getUTCMonth() + 1);
      const day = int(givenTime.getUTCDate());
      const year = givenTime.getUTCFullYear() % 100;
      return `${day}-${month}-${year}`;
    }
    const days = Math.floor((temp %= 31536000) / 86400);
    if (days) {
      if (days < 28) {
        return days + " day" + numberEnding(days);
      } else {
        const months = [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec"
        ];
        const month = months[givenTime.getUTCMonth()];
        const day = int(givenTime.getUTCDate());
        return `${day} ${month}`;
      }
    }
    const hours = Math.floor((temp %= 86400) / 3600);
    if (hours) {
      return `${hours} hour${numberEnding(hours)} ago`;
    }
    const minutes = Math.floor((temp %= 3600) / 60);
    if (minutes) {
      return `${minutes} minute${numberEnding(minutes)} ago`;
    }
    return "a few seconds ago";
  };
  return getTime();
}

function stringToInt(value: any, defValue: any = 0) {
  if (!value) {
    return 0;
  } else if (!isNaN(value)) {
    return parseInt(value, 10);
  }
  return defValue;
}
function stringToPositiveInt(value: any, defValue: number = 0) {
  const val = stringToInt(value, defValue);
  return val > -1 ? val : defValue;
}

export default {
  clearToken,
  getToken,
  timeDifference,
  stringToInt,
  stringToPositiveInt
};
