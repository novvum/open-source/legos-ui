import * as styledComponents from 'styled-components';
import { ThemedStyledComponentsModule } from 'styled-components';
import {
	borderRadius,
	boxShadow,
	transition,
	getPalette,
	getFonts
} from './styled-utils';

import { ThemeInterface } from './theme';

const {
	default: styled,
	css,
	injectGlobal,
	keyframes,
	ThemeProvider
} = styledComponents as ThemedStyledComponentsModule<ThemeInterface>;

export {
	css,
	injectGlobal,
	keyframes,
	ThemeProvider,
	borderRadius,
	boxShadow,
	transition,
	getPalette,
	getFonts
};
export default styled;
