import React from 'react';
import BoxTitleWrapper from '../BoxTitle';
import { BoxWrapper } from './styles';

export default (props: any) => (
	<BoxWrapper
		className={`${props.className} isoBoxWrapper`}
		style={props.style}
	>
		<BoxTitleWrapper title={props.title} subtitle={props.subtitle} />
		{props.children}
	</BoxWrapper>
);
