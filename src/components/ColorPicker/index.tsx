import React, { Component } from "react";
import Button from "../Button";
import Popover from "../Popover";
import ColorChooserDropdown from "./styles";

export default class extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.handleVisibleChange = this.handleVisibleChange.bind(this);
    this.hide = this.hide.bind(this);
    this.state = {
      visible: false
    };
  }
  hide() {
    this.setState({ visible: false });
  }
  handleVisibleChange() {
    this.setState({ visible: !this.state.visible });
  }
  render() {
    const { colors, selectedColor, changeColor } = this.props;
    const content = () => (
      <ColorChooserDropdown className="isoColorOptions">
        {colors.map((color: any, index: any) => {
          const onClick = () => {
            this.hide();
            changeColor(index);
          };
          const style = {
            background: color
          };
          return <Button key={index} onClick={onClick} style={style} />;
        })}
      </ColorChooserDropdown>
    );
    return (
      <Popover
        content={content()}
        trigger="click"
        visible={this.state.visible}
        onVisibleChange={this.handleVisibleChange}
      >
        <Button
          style={{ backgroundColor: colors[selectedColor] }}
          className="isoColorChooser"
        />
      </Popover>
    );
  }
}
