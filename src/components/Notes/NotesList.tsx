import React, { Component } from "react";
import { helpers, Scrollbar } from "../Utilities";
import Button from "../Button";
import { InputSearch } from "../Input";
import { NoteListWrapper } from "./styles";
function filterNotes(notes: any, search: any) {
  search = search.toUpperCase();
  if (search) {
    return notes.filter((note: any) =>
      note.note.toUpperCase().includes(search)
    );
  }
  return notes;
}
export default class extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.singleNote = this.singleNote.bind(this);
    this.onChange = this.onChange.bind(this);
    this.state = {
      search: ""
    };
  }
  singleNote(note: any) {
    const { selectedId, deleteNote, changeNote, colors } = this.props;
    const activeClass = selectedId === note.id ? "active" : "";
    const onChange = () => changeNote(note.id);
    const onDelete = () => deleteNote(note.id);
    return (
      <div className={`isoList ${activeClass}`} key={note.id}>
        <div
          className="isoNoteBGColor"
          style={{ width: "5px", background: colors[note.color] }}
        />
        <div className="isoNoteText" onClick={onChange}>
          <h3>{note.note}</h3>
          <span className="isoNoteCreatedDate">
            {helpers.timeDifference(note.createTime)}
          </span>
        </div>
        <Button
          className="isoDeleteBtn"
          icon="close"
          type="button"
          onClick={onDelete}
        />
      </div>
    );
  }
  onChange(event: any) {
    this.setState({ search: event.target.value });
  }
  render() {
    const { search } = this.state;
    const notes = filterNotes(this.props.notes, search);
    return (
      <NoteListWrapper className="isoNoteListWrapper">
        <InputSearch
          placeholder="Search Notes"
          className="isoSearchNotes"
          value={search}
          onChange={this.onChange}
        />
        <div className="isoNoteList">
          {notes && notes.length > 0 ? (
            <Scrollbar>
              {notes.map((note: any) => this.singleNote(note))}
            </Scrollbar>
          ) : (
            <span className="isoNoResultMsg">No note found</span>
          )}
        </div>
      </NoteListWrapper>
    );
  }
}
