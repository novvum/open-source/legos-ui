import React, { Component } from "react";
import { Provider, Consumer } from "./context";
import Notes from "./Notes";

export default class extends Component<any, any> {
  render() {
    return (
      <Provider>
        <Consumer>
          <Notes />
        </Consumer>
      </Provider>
    );
  }
}
