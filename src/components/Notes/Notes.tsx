import React, { Component } from "react";
import { Layout } from "antd";
import NoteList from "./NotesList";
import ColorPicker from "../ColorPicker";
import Button from "../Button";
import IntlMessage from "../IntlMessage";
import NoteComponentWrapper from "./styles";
import { Textarea } from "../Input";
const { Header, Content } = Layout;

// prettier-ignore

export default class Notes extends Component<any, any> {
	constructor(props: any) {
		super(props);
		this.updateNote = this.updateNote.bind(this);
	}
	updateNote(event: any) {
		const { editNote, selectedId } = this.props;
		this.props.editNote(selectedId, event.target.value);
	}
	render() {
		const {
			notes,
			selectedId,
			changeNote,
			deleteNote,
			addNote,
			colors,
			selectedColor,
			changeColor
		} = this.props;
		const selectedNote =
			selectedId !== undefined
				? notes.filter((note: any) => note.id === selectedId)[0]
				: null;
		return (
			<NoteComponentWrapper className='isomorphicNoteComponent'>
				<div style={{ width: '340px' }} className='isoNoteListSidebar'>
					<NoteList
						notes={notes}
						selectedId={selectedId}
						changeNote={changeNote}
						deleteNote={deleteNote}
						colors={colors}
					/>
				</div>
				<Layout className='isoNotepadWrapper'>
					<Header className='isoHeader'>
						{selectedId !== undefined ? (
							<div className='isoColorChooseWrapper'>
								<ColorPicker
									colors={colors}
									changeColor={changeColor}
									selectedColor={selectedColor}
								/>{' '}
								<span>
									<IntlMessage id='notes.chooseColor' />
								</span>
							</div>
						) : (
							''
						)}
						<Button type='primary' className='isoAddNoteBtn' onClick={addNote}>
							<IntlMessage id='notes.addNote' />
						</Button>
					</Header>
					<Content className='isoNoteEditingArea'>
						{selectedId !== undefined ? (
							<Textarea
								className='isoNoteTextbox'
								value={selectedNote.note}
								onChange={this.updateNote}
								autoFocus={true}
							/>
						) : (
							''
						)}
						{/*{selectedId !== undefined ? <span>{`created at ${selectedNote.createTime}`}</span> : ''}*/}
					</Content>
				</Layout>
			</NoteComponentWrapper>
		);
	}
}
