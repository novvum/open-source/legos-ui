import React from "react";
import { Colors } from "../Utilities";

const notes: any[] = [
  {
    id: 94926,
    note: "Sample Note",
    createTime: new Date().toISOString(),
    color: 0
  }
];

const DEFAULT_STATE = {
  notes,
  colors: [...Colors],
  selectedId: notes[0].id,
  selectedColor: notes[0].color
};

export const NotesContext = React.createContext({
  ...DEFAULT_STATE,
  changeNote: {},
  addNote: {},
  editNote: {},
  deleteNote: {},
  changeColor: {}
});
// prettier-ignore
class Provider extends React.Component<any, any> {
	state = DEFAULT_STATE;
	changeNote = (id: any) => {
		const notes = this.state.notes;
		const selected =
			notes[notes.findIndex((note: any) => note.id === id)].color;
		return this.setState({
			selectedId: id,
			selectedColor: selected
		});
	}
	addNote = () => {
		const newNote = {
			id: new Date(),
			note: 'New Note',
			createTime: new Date(),
			color: this.state.selectedColor
		};
		const notes = [newNote, ...this.state.notes];
		return this.setState({
			selectedId: newNote.id,
			notes
		});
	}
	editNote = (id: any, newNote: any) => {
		const oldNotes = this.state.notes;
		let notes: any[] = [];
		oldNotes.forEach((note: any) => {
			if (note.id !== id) {
				notes.push(note);
			} else {
				note.note = newNote;
				notes.push(note);
			}
		});
		return this.setState({
			notes
		});
	}
	deleteNote = (id: any) => {
		const oldNotes = this.state.notes;
		const notes: any[] = [];
		oldNotes.forEach((note: any) => {
			if (note.id !== id) {
				notes.push(note);
			}
		});
		let selectedId: any = this.state.selectedId;
		if (selectedId === id) {
			if (notes.length === 0) {
				selectedId = undefined;
			} else {
				selectedId = notes[0].id;
			}
		}
		return this.setState({
			notes,
			selectedId
		});
	}
	changeColor = (selectedColor: any) => {
		const oldNotes = this.state.notes;
		const selectedId = this.state.selectedId;
		const notes: any[] = [];
		oldNotes.forEach((note: any) => {
			if (note.id !== selectedId) {
				notes.push(note);
			} else {
				note.color = selectedColor;
				notes.push(note);
			}
		});
		return this.setState({
			notes,
			selectedColor
		});
	}

	render() {
		return (
				<NotesContext.Provider
					value={{
						...this.state,
						changeNote: this.changeNote,
						addNote: this.addNote,
						editNote: this.editNote,
						deleteNote: this.deleteNote,
						changeColor: this.changeColor
					}}
				>
					{this.props.children}
				</NotesContext.Provider>
		);
	}
}

class Consumer extends React.Component<any, any> {
  render() {
    const { children } = this.props;
    return (
      <NotesContext.Consumer>
        {({
          notes,
          colors,
          selectedId,
          selectedColor,
          changeNote,
          addNote,
          editNote,
          deleteNote,
          changeColor
        }) => {
          return React.Children.map(children, (child: any) =>
            React.cloneElement(child, {
              notes,
              colors,
              selectedId,
              selectedColor,
              changeNote,
              addNote,
              editNote,
              deleteNote,
              changeColor
            })
          );
        }}
      </NotesContext.Consumer>
    );
  }
}

export { Provider, Consumer };
