import { Tabs } from 'antd';
import AntTab from './styles';

const WDTabs = AntTab(Tabs);
const TabPane = Tabs.TabPane;
const isoTabs = WDTabs;

export default isoTabs;
export { TabPane };
