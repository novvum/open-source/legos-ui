import React, { Component } from 'react';
import Tabs, { TabPane } from '../Tabs';
import LayoutContentWrapper from '../LayoutWrapper';
import TableDemoStyle from './demo.style';
import { tableInfos } from './configs';
import * as TableViews from './tableViews';
import { FakeData } from '../Utilities';
let data = new FakeData(10);
export default class AntTable extends Component<any, any> {
	renderTable(tableInfo: any, dataList: any) {
		let Component;
		switch (tableInfo.value) {
			case 'sortView':
				Component = TableViews.SortView;
				break;
			case 'filterView':
				Component = TableViews.FilterView;
				break;
			case 'editView':
				Component = TableViews.EditView;
				break;
			case 'groupView':
				Component = TableViews.GroupView;
				break;
			case 'customizedView':
				Component = TableViews.CustomizedView;
				break;
			default:
				Component = TableViews.SimpleView;
		}
		return <Component {...{ tableInfo, dataList }} />;
	}
	render() {
		// prettier-ignore
		return (
			<LayoutContentWrapper>
				<TableDemoStyle className='isoLayoutContent'>
					<Tabs className='isoTableDisplayTab'>
						{tableInfos.map((tableInfo) => (
							<TabPane tab={tableInfo.title} key={tableInfo.value}>
								{this.renderTable(tableInfo, data)}
							</TabPane>
						))}
					</Tabs>
				</TableDemoStyle>
			</LayoutContentWrapper>
		);
	}
}
export { TableViews, tableInfos, data };
