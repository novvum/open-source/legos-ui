import styled, { getPalette } from '../styled-components';

const TableDemoStyle = styled.div`
	.ant-tabs-content {
		margin-top: 40px;
	}

	.ant-tabs-nav {
		> div {
			color: ${getPalette('secondary', 2)};

			&.ant-tabs-ink-bar {
				background-color: ${getPalette('primary', 0)};
			}

			&.ant-tabs-tab-active {
				color: ${getPalette('primary', 0)};
			}
		}
	}
`;

export default TableDemoStyle;
