import * as React from 'react';
const PendingPool = {};
const ReadyPool = {};

export default class ImageCell extends React.Component<any, any> {
	constructor(props: { src: string }) {
		super(props);
		this.loadImage = this.loadImage.bind(this);
		this.onLoadImage = this.onLoadImage.bind(this);
		this.state = {
			ready: false
		};
	}
	componentWillMount() {
		const props: any = this.props;
		this.loadImage(props.src);
	}
	componentWillReceiveProps(nextProps: any) {
		const props: any = this.props;
		if (nextProps.src !== props.src) {
			this.setState({ src: null });
			this.loadImage(nextProps.src);
		}
	}
	loadImage(src: any) {
		if (ReadyPool[src]) {
			this.setState({ src: src });
			return;
		}

		if (PendingPool[src]) {
			PendingPool[src].push(this.onLoadImage);
			return;
		}
		PendingPool[src] = [this.onLoadImage];

		const img = new Image();
		img.onload = () => {
			PendingPool[src].forEach((callback: any) => {
				callback(src);
			});
			delete PendingPool[src];
			img.onload = null;
			src = undefined;
		};
		img.src = src;
	}
	onLoadImage(src: any) {
		const props: any = this.props;
		ReadyPool[src] = true;
		if (src === props.src) {
			this.setState({
				src: src
			});
		}
	}
	render() {
		const state: any = this.state;
		const style: any = {
			backgroundImage: `url(${state.src})`,
			width: '70px',
			height: '70px',
			backgroundSize: 'cover'
		};

		return <div style={state.scr && style} />;
	}
}
