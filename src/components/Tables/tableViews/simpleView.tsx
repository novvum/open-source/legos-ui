import React, { Component } from 'react';
import TableWrapper from '../styles';

export default class SimpleView extends Component<any, any> {
	constructor(props: any) {
		super(props);
	}
	render() {
		const dataSource = this.props.dataSource || this.props.dataList.getAll();
		const columns = this.props.columns || this.props.tableInfo.columns;
		return (
			<TableWrapper
				pagination={false}
				columns={columns}
				dataSource={dataSource}
				className='isoSimpleTable'
			/>
		);
	}
}
