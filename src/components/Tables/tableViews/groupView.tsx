import React, { Component } from 'react';
import TableWrapper from '../styles';

export default class GroupView extends Component<any, any> {
	render() {
		return (
			<TableWrapper
				columns={this.props.tableInfo.columns}
				dataSource={this.props.dataList.getAll()}
				className='isoGroupTable'
			/>
		);
	}
}
