import React, { Component } from 'react';
import TableWrapper, { CustomizedTableWrapper } from '../styles';
import Switch from '../../Switch';
import Form from '../../Form';
const FormItem = Form.Item;

const expandedRowRender = (record: any) => (
	<p>{`${record.firstName} lives in ${record.city}`}</p>
);
const title = () => 'Here is title';
const showHeader = true;
const footer = () => 'Here is footer';
const scroll = { y: 240 };

const toggleOptions = [
	{
		defaultValue: true,
		title: 'Bordered',
		key: 'bordered'
	},
	{
		defaultValue: true,
		title: 'Loading',
		key: 'loading'
	},
	{
		defaultValue: true,
		title: 'Pagination',
		key: 'pagination'
	},
	{
		defaultValue: title,
		title: 'Title',
		key: 'title'
	},
	{
		defaultValue: showHeader,
		title: 'Show Header',
		key: 'showHeader'
	},
	{
		defaultValue: footer,
		title: 'Footer',
		key: 'footer'
	},
	{
		defaultValue: expandedRowRender,
		title: 'Expanded Row Render',
		key: 'expandedRowRender'
	},
	{
		defaultValue: {},
		title: 'Checkbox',
		key: 'rowSelection'
	},
	{
		defaultValue: scroll,
		title: 'Scrollable',
		key: 'scroll'
	}
];
export default class CustomizedView extends Component {
	constructor(props: any) {
		super(props);
		this.renderSwitch = this.renderSwitch.bind(this);
		this.state = {
			bordered: undefined,
			loading: undefined,
			pagination: true,
			size: 'default',
			expandedRowRender,
			title,
			showHeader,
			footer,
			rowSelection: {},
			scroll: undefined
		};
	}
	renderSwitch(option: any) {
		const checked = this.state[option.key] !== undefined;
		const onChange = () => {
			if (!checked) {
				this.setState({ [option.key]: option.defaultValue });
			} else {
				this.setState({ [option.key]: undefined });
			}
		};
		return (
			<FormItem label={option.title} key={option.key}>
				<Switch checked={checked} onChange={onChange} />
			</FormItem>
		);
	}
	render() {
		const props: any = this.props;
		return (
			<CustomizedTableWrapper className='isoCustomizedTableWrapper'>
				<div className='isoCustomizedTableControlBar'>
					<Form layout='inline'>
						{toggleOptions.map((option) => this.renderSwitch(option))}
					</Form>
				</div>
				<TableWrapper
					{...this.state}
					columns={props.tableInfo.columns}
					dataSource={props.dataList.getAll()}
					className='isoCustomizedTable'
				/>
			</CustomizedTableWrapper>
		);
	}
}
