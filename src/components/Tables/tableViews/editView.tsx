import React, { Component } from 'react';
import clone from 'clone';
import TableWrapper from '../styles';
import { EditableCell, DeleteCell } from '../helperCells';
// prettier-ignore
export default class CustomizedView extends Component<any, any> {
	constructor(props: any) {
		super(props);
		this.onCellChange = this.onCellChange.bind(this);
		this.onDeleteCell = this.onDeleteCell.bind(this);
		this.state = {
			columns: this.createColumns(clone(props.tableInfo.columns)),
			dataList: props.dataList.getAll()
		};
	}
	createColumns(columns: any) {
		const editColumnRender = (text: string, record: any, index: any) => (
			<EditableCell
				index={index}
				columnsKey={columns[0].key}
				value={text[columns[0].key]}
				onChange={this.onCellChange}
			/>
		);
		columns[0].render = editColumnRender;
		const deleteColumn = {
			title: 'operation',
			dataIndex: 'operation',
			render: (text: any, record: any, index: any) => (
				<DeleteCell index={index} onDeleteCell={this.onDeleteCell} />
			)
		};
		columns.push(deleteColumn);
		return columns;
	}
	onCellChange(value: any, columnsKey: any, index: any) {
		const { dataList } = this.state;
		dataList[index][columnsKey] = value;
		this.setState({ dataList });
	}
	onDeleteCell = (index: any) => {
		const { dataList } = this.state;
		dataList.splice(index, 1);
		this.setState({ dataList });
	}
	render() {
		const { columns, dataList } = this.state;

		return (
			<TableWrapper
				columns={columns}
				dataSource={dataList}
				className='isoEditableTable'
			/>
		);
	}
}
