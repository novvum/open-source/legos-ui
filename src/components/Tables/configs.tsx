import React from 'react';
import clone from 'clone';
import { DateCell, ImageCell, LinkCell, TextCell } from './helperCells';
import IntlMessage from '../IntlMessage';

const renderCell = (object: any, type: any, key: any) => {
	const value = object[key];
	switch (type) {
		case 'ImageCell':
			return ImageCell(value);
		case 'DateCell':
			return DateCell(value);
		case 'LinkCell':
			return LinkCell(value);
		default:
			return TextCell(value);
	}
};

const columns = [
	{
		title: <IntlMessage id='antTable.title.image' />,
		key: 'avatar',
		width: '1%',
		className: 'isoImageCell',
		render: (object: any) => renderCell(object, 'ImageCell', 'avatar')
	},
	{
		title: <IntlMessage id='antTable.title.firstName' />,
		key: 'firstName',
		width: 100,
		render: (object: any) => renderCell(object, 'TextCell', 'firstName')
	},
	{
		title: <IntlMessage id='antTable.title.lastName' />,
		key: 'lastName',
		width: 100,
		render: (object: any) => renderCell(object, 'TextCell', 'lastName')
	},
	{
		title: <IntlMessage id='antTable.title.city' />,
		key: 'city',
		width: 200,
		render: (object: any) => renderCell(object, 'TextCell', 'city')
	},
	{
		title: <IntlMessage id='antTable.title.street' />,
		key: 'street',
		width: 200,
		render: (object: any) => renderCell(object, 'TextCell', 'street')
	},
	{
		title: <IntlMessage id='antTable.title.email' />,
		key: 'email',
		width: 200,
		render: (object: any) => renderCell(object, 'LinkCell', 'email')
	},
	{
		title: <IntlMessage id='antTable.title.dob' />,
		key: 'date',
		width: 200,
		render: (object: any) => renderCell(object, 'DateCell', 'date')
	}
];
const smallColumns = [columns[1], columns[2], columns[3], columns[4]];
const sortColumns = [
	{ ...columns[1], sorter: true },
	{ ...columns[2], sorter: true },
	{ ...columns[3], sorter: true },
	{ ...columns[4], sorter: true }
];
const editColumns = [
	{ ...columns[1], width: 300 },
	{ ...columns[2], width: 300 },
	columns[3],
	columns[4]
];
const groupColumns = [
	columns[0],
	{
		title: 'Name',
		children: [columns[1], columns[2]]
	},
	{
		title: 'Address',
		children: [columns[3], columns[4]]
	}
];
const tableInfos = [
	{
		title: 'Simple Table',
		value: 'simple',
		columns: clone(smallColumns)
	},
	{
		title: 'Sortable Table',
		value: 'sortView',
		columns: clone(sortColumns)
	},
	{
		title: 'Search Text',
		value: 'filterView',
		columns: clone(smallColumns)
	},
	{
		title: 'Editable View',
		value: 'editView',
		columns: clone(editColumns)
	},
	{
		title: 'Grouping View',
		value: 'groupView',
		columns: clone(groupColumns)
	},
	{
		title: 'Customized View',
		value: 'customizedView',
		columns: clone(columns)
	}
];
export { columns, tableInfos };
