import React, { Component } from 'react';
import ImageCellView from './imageCell';
import { Icon, Input, Popconfirm } from 'antd';

const DateCell = (data: any) => <p>{data.toLocaleString()}</p>;
const ImageCell = (src: any) => <ImageCellView src={src} />;
const LinkCell = (link?: any, href?: any) => (
	<a href={href ? href : '#'}>{link}</a>
);
const TextCell = (text: string) => <p>{text}</p>;

class EditableCell extends Component<any, any> {
	constructor(props: any) {
		super(props);
		this.handleChange = this.handleChange.bind(this);
		this.check = this.check.bind(this);
		this.edit = this.edit.bind(this);
		this.state = {
			value: props.value,
			editable: false
		};
	}
	handleChange(event: any) {
		const value = event.target.value;
		this.setState({ value });
	}
	check() {
		const props: any = this.props;
		const state: any = this.state;
		this.setState({ editable: false });
		if (props.onChange) {
			props.onChange(state.value, props.columnsKey, props.index);
		}
	}
	edit() {
		this.setState({ editable: true });
	}
	render() {
		const state: any = this.state;
		const { value, editable } = state;
		return (
			<div className='isoEditData'>
				{editable ? (
					<div className='isoEditDataWrapper'>
						<Input
							value={value}
							onChange={this.handleChange}
							onPressEnter={this.check}
						/>
						<Icon type='check' className='isoEditIcon' onClick={this.check} />
					</div>
				) : (
					<p className='isoDataWrapper'>
						{value || ' '}
						<Icon type='edit' className='isoEditIcon' onClick={this.edit} />
					</p>
				)}
			</div>
		);
	}
}
class DeleteCell extends Component<any, any> {
	render() {
		const props: any = this.props;
		const { index, onDeleteCell } = props;
		return (
			<Popconfirm
				title='Sure to delete?'
				okText='DELETE'
				cancelText='No'
				onConfirm={() => onDeleteCell(index)}
			>
				<a>Delete</a>
			</Popconfirm>
		);
	}
}

export { DateCell, ImageCell, LinkCell, TextCell, EditableCell, DeleteCell };
