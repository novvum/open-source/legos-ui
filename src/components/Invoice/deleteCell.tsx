import React, { Component } from 'react';
import { Popconfirm } from 'antd';

export default class DeleteCell extends Component<any, any> {
	constructor(props: any) {
		super(props);
	}
	render() {
		const { index, onDeleteCell } = this.props;
		return (
			<Popconfirm
				title='Sure to delete?'
				okText='DELETE'
				cancelText='No'
				onConfirm={() => onDeleteCell(index)}
			>
				<a>Delete</a>
			</Popconfirm>
		);
	}
}
