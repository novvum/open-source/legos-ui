import { Switch } from 'antd';
import AntSwitch from './styles';

const Switches = AntSwitch(Switch);

export default Switches;
