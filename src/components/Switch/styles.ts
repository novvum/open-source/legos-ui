import styled, { getPalette } from '../styled-components';

const AntSwitch = (ComponentName: any) => styled(ComponentName)`
	&.ant-switch-checked {
		border-color: ${getPalette('primary', 0)};
		background-color: ${getPalette('primary', 0)};
	}
`;

export default AntSwitch;
