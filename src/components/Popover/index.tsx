import { Popover } from "antd";
import PopoverWrapper from "./styles";

export default PopoverWrapper(Popover);
