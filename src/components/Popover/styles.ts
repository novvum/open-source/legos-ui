import styled, { getPalette, borderRadius } from "../styled-components";

const PopoverWrapper = (ComponentName: any) => styled(ComponentName)`
  display: inline-block;

  .ant-popover {
    font-size: 13px;

    .ant-popover-inner {
      .ant-popover-title {
        background-color: ${getPalette("grayscale", 5)};
        font-size: 14px;
        font-weight: 500;
        color: ${getPalette("text", 0)};
        line-height: 35px;
        height: 35px;
        border-bottom: 1px solid ${getPalette("border", 0)};
        ${borderRadius("4px 4px 0 0")};
      }

      .ant-popover-inner-content {
        color: ${getPalette("text", 3)};

        a {
          color: ${getPalette("primary", 0)};
        }

        .ant-popover-message {
          padding: 8px 0 16px;
          font-size: 13px;
          color: ${getPalette("text", 1)};

          > .anticon {
            color: ${getPalette("warning", 0)};
            font-size: 14px;
            margin-top: 1px;
          }
        }
      }
    }

    &.ant-popover-placement-rightTop {
      > .ant-popover-content {
        > .ant-popover-arrow {
          background-color: transparent;
          &:after {
            border-right-color: ${getPalette("grayscale", 6)};
          }
        }
      }
    }

    &.ant-popover-placement-leftTop {
      > .ant-popover-content {
        > .ant-popover-arrow {
          background-color: transparent;
          &:after {
            border-left-color: ${getPalette("grayscale", 6)};
          }
        }
      }
    }

    &.ant-popover-placement-bottom,
    &.ant-popover-placement-bottomLeft,
    &.ant-popover-placement-bottomRight {
      > .ant-popover-content {
        > .ant-popover-arrow {
          background-color: transparent;
          &:after {
            border-bottom-color: ${getPalette("grayscale", 6)};
          }
        }
      }
    }
  }
`;

export default PopoverWrapper;
