import * as React from 'react';
import HttpErrorStyleWrapper from './styles';

export type HttpErrorProps = {
	code?: string;
	subtitle?: string;
	description?: string;
	linkText?: string;
	onClick: () => void;
	image: string;
};

function HttpError(props: HttpErrorProps) {
	return (
		<HttpErrorStyleWrapper className='iso404Page'>
			<div className='iso404Content'>
				<h1>{props.code}</h1>
				<h3>{props.subtitle}</h3>
				<p>{props.description}</p>
				<button type='button' onClick={() => props.onClick()}>
					<span className='isoMenuHolder'>
						<span className='nav-text'>{props.linkText}</span>
					</span>
				</button>
			</div>

			<div className='iso404Artwork'>
				<img alt='#' src={props.image} />
			</div>
		</HttpErrorStyleWrapper>
	);
}

export default HttpError;
