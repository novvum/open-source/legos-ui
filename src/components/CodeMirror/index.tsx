import React, { Component } from "react";
import { Row, Col } from "antd";
import Switch from "../Switch";
import Select, { SelectOption } from "../Select";
import Form from "../Form";
import PageHeader from "../PageHeader";
import Box from "../Box";
import LayoutWrapper from "../LayoutWrapper";
import ContentHolder from "../ContentHolder";
import { BasicStyles } from "../Utilities";
import { switchOptions, selectOptions, defaultValues } from "./config";
import CodeMirror, { CodeMirrorToolbar } from "./codeMirror/styles";

const FormItem = Form.Item;
const Option = SelectOption;

export default class CodeMirrorComponent extends Component<any, any, any> {
  constructor(props: any) {
    super(props);
    this.updateCode = this.updateCode.bind(this);
    this.toggleOptions = this.toggleOptions.bind(this);
    this.selectOptions = this.selectOptions.bind(this);
    this.state = {
      ...defaultValues,
      basicOptions: {
        lineNumbers: true,
        readOnly: false,
        tabSize: 4,
        mode: "javascript",
        theme: "zenburn"
      }
    };
  }
  updateCode(mode: any, value: any) {
    this.setState({
      [mode]: value
    });
  }
  toggleOptions() {
    const { basicOptions } = this.state;
    return switchOptions.map((option: any, index: any) => {
      const id = option.id;
      const onChange = () => {
        basicOptions[id] = !basicOptions[id];
        this.setState(basicOptions);
      };
      return (
        <FormItem label={option.title} key={option.id}>
          <Switch
            checked={option.value === basicOptions[id]}
            onChange={onChange}
          />
        </FormItem>
      );
    });
  }
  selectOptions() {
    const { basicOptions } = this.state;
    return selectOptions.map((option: any, index: any) => {
      const id = option.id;
      const handleChange = (value: any) => {
        basicOptions[id] = isNaN(value) ? value : parseInt(value, 10);
        this.setState(basicOptions);
      };
      return (
        <FormItem label={option.title} key={option.id}>
          <Select defaultValue={`${basicOptions[id]}`} onChange={handleChange}>
            {option.options.map((opt: any) => (
              <Option value={opt} key={opt}>
                {opt}
              </Option>
            ))}
          </Select>
        </FormItem>
      );
    });
  }
  render() {
    const { rowStyle, colStyle, gutter } = BasicStyles;
    return (
      <LayoutWrapper>
        <PageHeader>Code Mirror</PageHeader>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Box title="Basic Example">
              <ContentHolder>
                <CodeMirrorToolbar className="isoOptionWrapper">
                  {this.toggleOptions()}
                  {this.selectOptions()}
                </CodeMirrorToolbar>
                <CodeMirror
                  value={this.state.basic}
                  onChange={(value: any) => this.updateCode("basic", value)}
                  options={this.state.basicOptions}
                />
              </ContentHolder>
            </Box>
          </Col>
        </Row>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={12} sm={12} xs={24} style={colStyle}>
            <Box title="Ruby Example">
              <ContentHolder>
                <CodeMirror
                  value={this.state.ruby}
                  onChange={(value: any) => this.updateCode("ruby", value)}
                  options={{ lineNumbers: true, theme: "hopscotch" }}
                />
              </ContentHolder>
            </Box>
          </Col>
          <Col md={12} sm={12} xs={24} style={colStyle}>
            <Box title="Javascript Example">
              <ContentHolder>
                <CodeMirror
                  value={this.state.javascript}
                  onChange={(value: any) =>
                    this.updateCode("javascript", value)
                  }
                  options={{ lineNumbers: true, theme: "twilight" }}
                />
              </ContentHolder>
            </Box>
          </Col>
        </Row>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={12} sm={12} xs={24} style={colStyle}>
            <Box title="Markdown Example">
              <ContentHolder>
                <CodeMirror
                  value={this.state.markdown}
                  onChange={(value: any) => this.updateCode("markdown", value)}
                  options={{ lineNumbers: true, theme: "rubyblue" }}
                />
              </ContentHolder>
            </Box>
          </Col>
          <Col md={12} sm={12} xs={24} style={colStyle}>
            <Box title="XML Example">
              <ContentHolder>
                <CodeMirror
                  value={this.state.xml}
                  onChange={(value: any) => this.updateCode("xml", value)}
                  options={{ lineNumbers: true, theme: "paraiso-dark" }}
                />
              </ContentHolder>
            </Box>
          </Col>
        </Row>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={12} sm={12} xs={24} style={colStyle}>
            <Box title="PHP Example">
              <ContentHolder>
                <CodeMirror
                  value={this.state.php}
                  onChange={(value: any) => this.updateCode("php", value)}
                  options={{ lineNumbers: true, theme: "midnight" }}
                />
              </ContentHolder>
            </Box>
          </Col>
          <Col md={12} sm={12} xs={24} style={colStyle}>
            <Box title="Python Example">
              <ContentHolder>
                <CodeMirror
                  value={this.state.python}
                  onChange={(value: any) => this.updateCode("python", value)}
                  options={{ lineNumbers: true, theme: "material" }}
                />
              </ContentHolder>
            </Box>
          </Col>
        </Row>
      </LayoutWrapper>
    );
  }
}
