import React from 'react';
import { ComponentTitleWrapper } from './styles';

export default (props: any) => (
	<ComponentTitleWrapper className='isoComponentTitle'>
		{props.children}
	</ComponentTitleWrapper>
);
