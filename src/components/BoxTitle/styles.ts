import styled, { getPalette } from '../styled-components';

const BoxTitle = styled.h3`
	font-size: 14px;
	font-weight: 500;
	color: ${getPalette('text', 0)};
	margin: 0;
	margin-bottom: 5px;
`;

const BoxSubTitle = styled.p`
	font-size: 13px;
	font-weight: 400;
	color: ${getPalette('text', 3)};
	line-height: 24px;
`;

export { BoxTitle, BoxSubTitle };
